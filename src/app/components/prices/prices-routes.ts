import { Routes } from "@angular/router";
import { PrimerPriceComponent } from "./primer-price.component";
import { SegundoPriceComponent } from "./segundo-price.component";
import { TercerPriceComponent } from "./tercer-price.component";


export const Usuario_Routes: Routes =[
  { path: '20/:parametro2', component: PrimerPriceComponent },
  { path: '50', component: SegundoPriceComponent },
  { path: '80', component: TercerPriceComponent },
  { path: '**', pathMatch: 'full', redirectTo: '20' }
]